import argparse
from user_lib.snmp import SnmpConnector

AUTHOR = 'Belim S.'
EMAIL = 'bullit88@mail.ru'
PROGRAM = 'Flows count checker'
DESCRIPTION = 'Check flows count by OID'
VERSION = '0.1'

# oid = '1.3.6.1.2.1.1.1.0'
oid = '1.3.6.1.2.1.10.20.1.2.1.1.2'


def main_f():
    parser = argparse.ArgumentParser(description="%s (v%s)" % (PROGRAM, str(VERSION)))
    parser.add_argument("--all", action="store_true",
                        help="all flows count")
    parser.add_argument("--active", action="store_true",
                        help="only active flows count")

    namespace = parser.parse_args()

    c = SnmpConnector()
    error, data = c.get_oid_cmd(oid)

    if namespace.all:
        return error if error else len(data)

    elif namespace.active:
        return error if error else [i.prettyPrint() for i in data].count('4')

    else:
        parser.print_help()
        return None


if __name__ == '__main__':
    print(main_f())
