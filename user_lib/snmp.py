from pysnmp import debug
from pysnmp.error import PySnmpError
import pysnmp.hlapi as hlp


class SnmpResponse(list):
    """Класс ответа SMNP"""
    def __init__(self, code, data):
        """
        :param code:
            код возврата
        :param data:
            данные возврата
        """
        super().__init__(tuple([code, data]))
        self.code = code
        self.data = data

    def __repr__(self):
        return '''<<SMNP Responce: [%s, %s]>>''' % (self.code, self.data)

    def __str__(self):
        return self.__repr__()


class SnmpConnector:
    """Класс подключения SNMP"""

    default_community = 'public'
    default_address = 'demo.snmplabs.com'
    default_port = 161
    default_debug_status = False

    _NO_EXCEPT, _ENG_EXCEPT, _PDU_EXCEPT, _EXC_EXCEPT, _SNMP_EXCEPT, _FAIL_OID_EXCEPT = range(0, -6, -1)

    _log_dict = {_NO_EXCEPT: 'all OK with OID "%s"',
                 _ENG_EXCEPT: 'get error by engine: %s',
                 _PDU_EXCEPT: 'get pdu error: %s',
                 _EXC_EXCEPT: 'exception while executing: %s',
                 _SNMP_EXCEPT: 'error occurred while performing SNMP operation: %s',
                 _FAIL_OID_EXCEPT: 'sended incorrect OID: "%s"'}

    def __init__(self, **kwargs):
        """
        :param kwargs:
            community
                комьюнити (str)
            address
                адрес для подключения (str)
            port
                порт для подключения (int)
            debug
                логирование (bool) (default: False)
        """

        self.community = kwargs.get('community', self.default_community)
        self.address = kwargs.get('address', self.default_address)
        self.port = kwargs.get('port', self.default_port)
        self.debug = kwargs.get('debug', self.default_debug_status)

        self.payload = []

        if self.debug:
            debug.setLogger(debug.Debug('dsp', 'msgproc', 'secmod'))

    def __repr__(self):
        return '''<<SMNP:%s@%s:%s>>''' % (self.community, self.address, self.port)

    def __str__(self):
        return self.__repr__()

    def _log_returner(self, key, *args):
        """Метод записи в лог"""

        if not self.debug:
            return

        if args:
            debug.logger(self._log_dict[key] % str(args[0]))
            return

        debug.logger(self._log_dict[key])

    def _failed_oid(self):
        """Метод проверки существования OID"""
        return str(self.payload[0].prettyPrint()).endswith('No Such Instance currently exists at this OID')

    def get_oid_cmd(self, c_oid):
        """
        Метод получения pysnmp.smi.rfc1902.ObjectType по OID
        :param c_oid:
            OID для запроса (str)
        :return:
            except_status, tuple(pysnmp.smi.rfc1902.ObjectType, )
        """
        try:
            try:
                engine_error_indication, pdu_error_indication, _, self.payload = hlp.getCmd(
                    hlp.SnmpEngine(),
                    hlp.CommunityData(self.community),
                    hlp.UdpTransportTarget((self.address, self.port)),
                    hlp.ContextData(),
                    hlp.ObjectType(hlp.ObjectIdentity(c_oid))).__next__()

            except PySnmpError as e:
                self._log_returner(self._SNMP_EXCEPT, e)
                return SnmpResponse(self._SNMP_EXCEPT, self.payload)

            else:
                if engine_error_indication:
                    self._log_returner(self._ENG_EXCEPT, engine_error_indication.prettyPrint())
                    return SnmpResponse(self._ENG_EXCEPT, self.payload)

                if pdu_error_indication:
                    self._log_returner(self._PDU_EXCEPT, pdu_error_indication.prettyPrint())
                    return SnmpResponse(self._PDU_EXCEPT, self.payload)

                if self._failed_oid():
                    self._log_returner(self._FAIL_OID_EXCEPT, c_oid)
                    return SnmpResponse(self._FAIL_OID_EXCEPT, self.payload)

                self._log_returner(self._NO_EXCEPT, c_oid)
                return SnmpResponse(self._NO_EXCEPT, self.payload)

        except Exception as e:
            self._log_returner(self._EXC_EXCEPT, e)
            return SnmpResponse(self._EXC_EXCEPT, self.payload)

